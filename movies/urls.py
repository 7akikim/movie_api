from django.urls import path
from . import views
# app_name = 'movies'

urlpatterns = [
    path('movies/', views.movies_list),
    path('movies/<int:movie_pk>/', views.movies),
    path('genres/', views.genres_list),
    path('genres/<int:genre_pk>/', views.genres),
    path('movies/<int:movie_pk>/scores/', views.movies_scores),
    path('scores/<int:score_pk>', views.scores),
]
