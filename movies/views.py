from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import MovieSerializer, GenreSerializer, GenreDetailSerializer, ScoreSerializer
from .models import Movie, Genre, Score

@api_view(['GET'])
def movies_list(request):
    movies = Movie.objects.all()
    serializer = MovieSerializer(movies, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def movies(request, movie_pk):
    movie = get_object_or_404(Movie, pk=movie_pk)
    serializer = MovieSerializer(movie)
    return Response(serializer.data)

@api_view(['GET'])
def genres_list(request):
    genres = Genre.objects.all()
    serializer = GenreSerializer(genres, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def genres(request, genre_pk):
    genre = get_object_or_404(Genre, pk=genre_pk)
    serializer = GenreDetailSerializer(genre)
    return Response(serializer.data)

@api_view(['POST'])
def movies_scores(request, movie_pk):
    movie = get_object_or_404(Movie, pk=movie_pk)
    serializer = ScoreSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save(movie=movie)
        return Response({'message': 'Score was set!'})

@api_view(['PUT', 'DELETE'])
def scores(request, score_pk):
    score = get_object_or_404(Score, pk=score_pk)
    if request.method == 'PUT':
        serializer = ScoreSerializer(data=request.data, instance=score)
        if serializer.is_valid(raise_exception=True):
            score = serializer.save()
            return Response({'message': 'Score was edited!'})
    else:
        score.delete()
        return Response({'message': 'Score was deleted!'})
    