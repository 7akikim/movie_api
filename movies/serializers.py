from rest_framework import serializers
from .models import Movie, Genre, Score

class ScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Score
        # fields = '__all__'
        exclude = ('movie', )

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'
        
class GenreSerializer(serializers.ModelSerializer):
    movies_count = serializers.IntegerField(
        source='movies.count')
    class Meta:
        model = Genre
        fields = '__all__'

class GenreDetailSerializer(serializers.ModelSerializer):
    movies = MovieSerializer(many=True)
    class Meta:
        model = Genre
        fields = '__all__'
